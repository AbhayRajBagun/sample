package com.sample.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sample.demo.model.Student;

public interface StudentDao extends JpaRepository<Student, Integer>{
	
	//@Query("SELECT s FROM Students WHERE s.name LIKE %:substring%")
	public List<Student> nameContaining(String substring);
}
