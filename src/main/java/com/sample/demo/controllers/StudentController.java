package com.sample.demo.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sample.demo.Dao.StudentDao;
import com.sample.demo.model.Student;

@RestController
public class StudentController {
	
	@Autowired
	private StudentDao sd;
	
	@GetMapping("students")
	public Iterable<Student> getAll() {
		return sd.findAll();
	}
	
	@PostMapping("save")
	public String save(@RequestBody Student s) {
		sd.save(s);
		return "Success!";
	}
	
	@GetMapping("search")
	public List<Student> search(@RequestParam("substring") String subString) {
		return sd.nameContaining(subString);
	}
	
}
